'''
Take picture and save it in local folder
'''

import io
import picamera
import numpy
import cv2

image_name = input("Please choose image name: \n")

#Create a memory stream so photos doesn't need to be saved in a file
stream = io.BytesIO()

with picamera.PiCamera() as camera:
    camera.resolution = (640, 480)
    camera.capture(stream, format='jpeg')
    
#Convert the picture into a numpy array
buff = numpy.frombuffer(stream.getvalue(), dtype=numpy.uint8)

#Now creates an OpenCV image
image = cv2.imdecode(buff, 1)

#Save the result image
cv2.imwrite(f'./{image_name}.jpg',image)

'''
capturing to file driectly
import time
import picamera

with picamera.PiCamera() as camera:
    camera.resolution = (1024, 768)
    camera.start_preview()
    # Camera warm-up time
    time.sleep(2)
    camera.capture('image.jpg')
'''
